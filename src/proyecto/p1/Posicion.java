/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.p1;

/**
 *
 * @author Raul  Villao, Xavier Garcia, Anthony Barco
 */
public class Posicion {
    int x;
    int y;
    
    public Posicion(int x, int y){
        this.x=x;
        this.y=y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
}

