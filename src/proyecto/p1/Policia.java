
package proyecto.p1;

import java.util.ArrayList;
import universo.Universo;
import java.util.Random;
public class Policia extends Persona {
    
    public Policia(int posicionX, int posicionY){
        super(posicionX,posicionY);
    }
    
    public String toString(){
        return "Pol";
    }
   
    
    public void encarcelar(){
        int x=getPosicionX();
        int y=getPosicionY();
        int vision = getVision();
        Random r1= new Random();
        int aleatorio= r1.nextInt(vision)+1;
        Random r2=new Random();
        int direccion= r2.nextInt(8);
        Persona encarcelada;
        switch (direccion){
            case 0:
                if (y==(Universo.getDimension()-1)){
                    break;
                }
                y+=aleatorio;
                if(y>(Universo.getDimension()-1)){
                    break;
                }
                encarcelada=Universo.matriz[x][y];
                if (encarcelada instanceof Ciudadano && ((Ciudadano)encarcelada).isRevelado()){
                    Universo.carcel.add(encarcelada);
                    Persona per= null;
                    Universo.matriz[x][y]= per;
                }   
                break;
                
                
            case 1:
                if (y==0){
                    break;
                }
                y=y-aleatorio;
                if (y<0){
                    break;
                }
                encarcelada=Universo.matriz[x][y];
                if (encarcelada instanceof Ciudadano && ((Ciudadano)encarcelada).isRevelado()){
                    Universo.carcel.add(encarcelada);
                    Persona per= null;
                    Universo.matriz[x][y]=per;
                }   
                break;
                
                
            case 2:
                if(x==(Universo.getDimension()-1)){
                    break;
                }
                x+=aleatorio;
                if(x>(Universo.getDimension()-1)){
                    break;
                }
                encarcelada=Universo.matriz[x][y];
                if (encarcelada instanceof Ciudadano && ((Ciudadano)encarcelada).isRevelado()){
                    Universo.carcel.add(encarcelada);
                    Persona per= null;
                    Universo.matriz[x][y]=per;
                }   
                break;
                
                
            case 3:
                if(x==0){
                    break;
                }
                x=x-aleatorio;
                if (x<0){
                    break;
                }
                encarcelada=Universo.matriz[x][y];
                if (encarcelada instanceof Ciudadano && ((Ciudadano)encarcelada).isRevelado()){
                    Universo.carcel.add(encarcelada);
                    Persona per= null;
                    Universo.matriz[x][y]=per;
                }   
                break;
                
                
            case 4:
                if(y==0 || x==0){
                    break;
                }
                x=x-aleatorio;
                y=y-aleatorio;
                if (x<0 || y<0){
                    break;
                }
                encarcelada=Universo.matriz[x][y];
                if (encarcelada instanceof Ciudadano && ((Ciudadano)encarcelada).isRevelado()){
                    Universo.carcel.add(encarcelada);
                    Persona per= null;
                    Universo.matriz[x][y]=per;
                }   
                break;
                
                
            case 5:
                if(y==(Universo.getDimension()-1) || x==0){
                    break;
                }
                x=x-aleatorio;
                y+=aleatorio;
                if (x<0 || y>(Universo.getDimension()-1)){
                    break;
                }
                encarcelada=Universo.matriz[x][y];
                if (encarcelada instanceof Ciudadano && ((Ciudadano)encarcelada).isRevelado()){
                    Universo.carcel.add(encarcelada);
                    Persona per= null;
                    Universo.matriz[x][y]=per;
                }   
                break;
                
                
            case 6:
                if(y==(Universo.getDimension()-1) || x==(Universo.getDimension()-1)){
                    break;
                }
                x+=aleatorio;
                y+=aleatorio;
                if(x>(Universo.getDimension()-1) || y>(Universo.getDimension()-1)){
                    break;
                }
                encarcelada=Universo.matriz[x][y];
                if (encarcelada instanceof Ciudadano && ((Ciudadano)encarcelada).isRevelado()){
                    Universo.carcel.add(encarcelada);
                    Persona per= null;
                    Universo.matriz[x][y]=per;
                }   
                break;
                
                
            case 7:
                if (y==0 || x==(Universo.getDimension()-1)){
                    break;
                }
                x+=aleatorio;
                y=y-aleatorio;
                if(x>Universo.getDimension()-1 || y<0){
                    break;
                }
                encarcelada=Universo.matriz[x][y];
                if (encarcelada instanceof Ciudadano && ((Ciudadano)encarcelada).isRevelado()){
                    Universo.carcel.add(encarcelada);
                    Persona per= null;
                    Universo.matriz[x][y]=per;
                }   
                
                break;
        }
    }
    

 
}
