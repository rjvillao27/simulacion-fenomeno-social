
package proyecto.p1;

/**
 *
 * @author Raúl Villao, Xavier Garcia, Anthony Barco
 */

import universo.Universo;
import java.util.Scanner;
import java.io.FileNotFoundException;


public class Principal {

    public static Scanner sc = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        menu();
    }
    
    
    public static void menu() throws FileNotFoundException{
        Universo u= new Universo(0);
        System.out.println("-----------SIMULACIÓN DE CONFLICTOS SOCIALES-----------"
                + "\n" + "A continuación ingrese los parametros solicitados para la simulación.");
        System.out.println("1.- Establecer tamaño del universo");
        generarUniverso(u);
        System.out.println("2.- Establecer densidades de poblacion y policías");
        establecerDensidades(u);
        System.out.println("3.- Establecer visión de los agentes");
        establecerVision(u);
        System.out.println("4.- Establecer número de turnos");  
        establecerTurnos(u);
        System.out.println("5.- Establecer legitimidad del gobierno");
        establecerLegitimidad(u);
        System.out.println("6.- Movimiento");
        establecerMovimiento(u);
        System.out.println("Empezando Simulación......");
        empezarSimulacion(u);
        }    
    
    public static void generarUniverso(Universo u){
        System.out.println("Ingrese el tamaño del universo, recuerde que la dimension es de nxn así que solo ingrese un valor entero positivo como máximo 50");
        String tamano= sc.nextLine();
        int t= Integer.parseInt(tamano);
        u.setDimension(t);
    }
 
    public static void establecerDensidades(Universo u){
        boolean respuesta=true;
        while (respuesta){
            System.out.println("Ingrese las densidad de la población: "
                + "Recuerde que la densidad poblacional y la de policías deben ser menores o iguales a un 100%");
            String poblacion= sc.nextLine();
            int pob= Integer.parseInt(poblacion);
            System.out.println("Ingrese las densidad de los policías: "
                + "Recuerde que la densidad poblacional y la de policías deben ser menores o iguales a un 100%");
            String policias= sc.nextLine();
            int pol= Integer.parseInt(policias);
            if (pol+pob<=100){
                u.setDensidadCiu(pob);
                u.setDensidadPol(pol);
                respuesta=false;
            } else{
                System.out.println("Valores ingresados no suman el 100% ni son menores, vuelva a ingresar los valores");
                respuesta=true;
            }
                
        }
    }
    
    public static void establecerVision(Universo u){
        System.out.println("Ingrese el campo de visión que tendrá cada agente:");
        String vision= sc.nextLine();
        int v= Integer.parseInt(vision);
        u.setVision(v);
    }
    
    public static void establecerTurnos(Universo u){
        System.out.println("Ingrese el numero de días en que se realizara la simulación:");
        String dias= sc.nextLine();
        int d= Integer.parseInt(dias);
        u.setDias(d);
    }
    
    public static void establecerLegitimidad(Universo u){
        boolean respuesta=true;
        while(respuesta){
            System.out.println("Ingrese la legitimidad que desee para el gobierno del universo que sea entre 0 y 1:");
            String legitimidad= sc.nextLine();
            double l = Double.parseDouble(legitimidad);
            if (l<=1  && l>=0){
                u.setLegitimidad(l);
                respuesta=false;
            }else{
                System.out.println("La legitimidad no esta dentro del rango permitido, vuelva a ingresarla");
                respuesta=true;
            }  
        }   
    }

    public static void establecerMovimiento(Universo u){
        boolean respuesta=true;
        while(respuesta){
            System.out.println("Desea que los agentes esten en movimiento?: S/N");
            String texto= sc.nextLine();
            if (texto.equals("S") || texto.equals("s")){
                u.setMovimiento(true);
                respuesta=false;
                break;
            } else if(texto.equals("N") || texto.equals("n")){
                u.setMovimiento(false);
                respuesta=false;
                break;
            } else{
                System.out.println("No ha ingresado una respuesta válida, ingrese nuevamente el movimiento.");
                respuesta=true;
            }
        }
    }
     
    public static void empezarSimulacion(Universo u) throws FileNotFoundException{
        u.comenzar();
    }
    
    
}
    
    
    
    
    

