
package proyecto.p1;

import java.util.ArrayList;
import universo.Universo;


public abstract class Persona {
    private int posicionX;
    private int posicionY;
    public final double K=2.3; 
    public static int vision= Universo.getVision();

    public Persona(int posicionX, int posicionY) {
        this.posicionX = posicionX;
        this.posicionY = posicionY;
    }

    public int getPosicionX() {
        return posicionX;
    }

    public int getPosicionY() {
        return posicionY;
    }

    public void setPosicionX(int posicionX) {
        this.posicionX = posicionX;
    }

    public void setPosicionY(int posicionY) {
        this.posicionY = posicionY;
    }

    public static int getVision() {
        return vision;
    }
    
    @Override
    public abstract String toString();

    

    
}
