
package proyecto.p1;

import java.util.ArrayList;
import universo.Universo;
import java.lang.Math;

public class Ciudadano extends Persona{
    private boolean revelado=false;
    private boolean preso;
    private double aversion; //aversion al riesgo aleatorio
    private double agravio; 
    private double perjuicio; // aleatorio (0-100)
    
    
    public Ciudadano(int posicionX, int posicionY){
        super(posicionX,posicionY);
        aversion= Math.random()*(-100)+100;
        perjuicio= Math.random()*(-100)+100;
        preso=false;
        
    }
    
    public double calcAgravio(){
        return agravio= (1-Universo.legitimidad)*perjuicio;
    }
    
    public double calcProbDetension(){
        double detencion=1-(Math.exp((-K)*(Math.round(Universo.getDensidadPol()/Universo.getDensidadCiu()))));
        return detencion;
    }
    
    public double riesgoNeto(){
        double riesgoN;
        riesgoN=(this.aversion*calcProbDetension());
        return riesgoN;
    }
    public double riesgoNetoDos(){
        double riesgoN;
        riesgoN=(this.aversion*calcProbDetensionDos());
        return riesgoN;
    }
    public double calcProbDetensionDos(){
        if (Universo.numeroCiudadanoR()!=0){
            double detencion=1-(Math.exp((-K)*(Math.round(Universo.getDensidadPol()/Universo.numeroCiudadanoR()))));
            return detencion;
        }else{
            return calcProbDetension();
        }  
    }
    
    public void setRevelarse(){
        if (Universo.getDiaActual()==1){
            double valor= calcAgravio()-riesgoNeto();
            if (valor>Universo.LIMITE){
                this.revelado= true;
            } else{
                this.revelado=false;
            }
        } else{
            double valor= calcAgravio()-riesgoNetoDos();
            if (valor>Universo.LIMITE){
                this.revelado= true;
            } else{
                this.revelado=false;
            }
            
        }
    }
    
    public boolean isRevelado() {
        return revelado;
    }

    public boolean isPreso() {
        return preso;
    }

    public double getAversion() {
        return aversion;
    }

    public double getAgravio() {
        return agravio;
    }
    
    @Override
    public String toString(){
       if (isRevelado()){
          return "CiuR" ; 
       }else{
           return "Ciu";
       }
        
    }

 
}
