/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universo;

/**
 *
 * @author Raul Villao,Xavier Garcia, Anthony Barco
 */
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;
import static proyecto.p1.Principal.sc;
import proyecto.p1.Persona;
import proyecto.p1.Ciudadano;
import proyecto.p1.Policia;
import proyecto.p1.Posicion;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;
import java.text.SimpleDateFormat;


public class Universo {
    private static int densidadPol=0;
    private static int densidadCiu=0;
    private static int dimension;
    private static int vision=0; 
    private int dias=0; 
    public static double legitimidad=0.0;
    private boolean movimiento=true;
    public static final double LIMITE=0.1; 
    private double ProbDeteccion=0.0;
    public double RiesgoNeto=0.0;
    public static ArrayList<Persona> carcel= new ArrayList<> ();
    public static Persona matriz[][];
    private static int diaActual=0;
    

    public Universo(int dimension) {
        this.dimension=dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public static int getDimension() {
        return dimension;
    }

    public static int getVision() {
        return vision;
    }
      
    public void setDensidadPol(int densidadPol) {
        this.densidadPol = densidadPol;
    }

    public static int getDensidadPol() {
        int numpol;
        double num=(densidadPol*(dimension*dimension));
        num=num/100;
        numpol= (int)num;
        return numpol;
    }

    public static int getDensidadCiu() {
        int numciud;
        double num=(densidadCiu*(dimension*dimension));
        num=num/100;
        numciud=(int)num;
        return numciud;
    }

    public void setDensidadCiu(int densidadCiu) {
        this.densidadCiu = densidadCiu;
    }

    public void setVision(int vision) {
        this.vision = vision;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }

    public int getDias() {
        return dias;
    }

    public static void setDiaActual(int diaActual) {
        Universo.diaActual = diaActual;
    }

    public static int getDiaActual() {
        return diaActual;
    }

    public void setLegitimidad(double legitimidad) {
        this.legitimidad = legitimidad;
    }

    public static double getLegitimidad() {
        return legitimidad;
    }

    public boolean isMovimiento() {
        return movimiento;
    }
  
    public void setMovimiento(boolean movimiento) {
        this.movimiento = movimiento;
    }
    
    
    public static int numeroCiudadanoR(){
        ArrayList<Ciudadano> conjunto= new ArrayList<>();
        for (int x=0; x < matriz.length; x++) {
          for (int y=0; y < matriz[x].length; y++) {
            if (matriz[x][y] instanceof Ciudadano){
                Ciudadano ciu= (Ciudadano)matriz[x][y];
                if(ciu.isRevelado()){
                   conjunto.add(ciu); 
                }
                
            }
          }
        }
        return conjunto.size();
    }

    public static int numeroCiudadanoNR(){
        ArrayList<Ciudadano> conjunto= new ArrayList<>();
        for (int x=0; x < matriz.length; x++) {
          for (int y=0; y < matriz[x].length; y++) {
            if (matriz[x][y] instanceof Ciudadano){
                Ciudadano ciu= (Ciudadano)matriz[x][y];
                if(ciu.isRevelado()== false){
                   conjunto.add(ciu); 
                }
                
            }
          }
        }
        return conjunto.size();
    }

    public void comenzar() throws FileNotFoundException{
        int n=getDimension();
        Persona matriz[][]= new Persona[n][n];
        ArrayList<Persona> policias=new ArrayList<>();
        ArrayList<Persona> ciudadanos=new ArrayList<>();
        ArrayList<Posicion> posiciones= new ArrayList<>();
        for (int i=1; i<=getDensidadPol();i++){//Con este For generamos el numero de policias ya establecidos, todos con una pocicion distinta
            generadorPolicia(policias, posiciones, n);      
        }
        for (int i=1; i<=getDensidadCiu();i++){ //Con este for generamos los ciudadanos con posiciones diferentes entre ellos e incluso diferentes de los policias, acorde a la densidad establesida
            generadorCiudadano(ciudadanos, posiciones,n);
        }
        //A continuación ubicamos a cada ciudadanos en la matriz
        for (Persona ciu:ciudadanos){
            int x=ciu.getPosicionX();
            int y=ciu.getPosicionY();
            matriz[x][y]=ciu;
        }
        //A continuación ubicamos a cada policia en la matriz
        for (Persona pol:policias){
            int x=pol.getPosicionX();
            int y=pol.getPosicionY();
            matriz[x][y]=pol;
        }
        this.matriz=matriz;
        
        /*for (Persona pol:policias){
            System.out.println(pol+ ":"+pol.getPosicionX()+","+pol.getPosicionY());
        }

        for (Persona ciu:ciudadanos){
            System.out.println(ciu+ ":"+ciu.getPosicionX()+","+ciu.getPosicionY());
        }
        */
        
        
        
        
        
        //Aquí obtenemos las fechas para los nombres de los archvivos que se generarna
        Date fecha= new Date();
        SimpleDateFormat formato= new SimpleDateFormat("ddMMyyyyHHmmss");
        String date= formato.format(fecha);
        String nombre="parametros_"+date;
        String nombre2="simulación_"+date;
        
        //Generamos Primero el archivo de parametros deseados
        PrintWriter pw = new PrintWriter(new File(nombre+".csv"));
        StringBuilder sb = new StringBuilder();
        sb.append("Tamano del Universo");
        sb.append(',');
        sb.append("Numero de ciudadanos");
        sb.append(',');
        sb.append("Numero de policias");
        sb.append(',');
        sb.append("Numero de turnos");
        sb.append(',');
        sb.append("Legitimidad");
        sb.append(',');
        sb.append("Movimientos");
        sb.append('\n');

        sb.append(getDimension()+"x"+getDimension());
        sb.append(',');
        sb.append(getDensidadCiu());
        sb.append(',');
        sb.append(getDensidadPol());
        sb.append(',');
        sb.append(getDias());
        sb.append(',');
        sb.append(getLegitimidad());
        sb.append(',');
        sb.append(isMovimiento());
        sb.append('\n');

        pw.write(sb.toString());
        pw.close();
        System.out.println("Archivo de parametros generado");
        
        
        //Generamos el archivo de simulación
        PrintWriter ak = new PrintWriter(new File(nombre2+".csv"));
        StringBuilder es = new StringBuilder();
        es.append("Numero de Turno");
        es.append(',');
        es.append("Numero de Agentes Tranquilos");
        es.append(',');
        es.append("Numero de Agentes rebelandose");
        es.append(',');
        es.append("Numero de agentes en la carcel");
        es.append('\n');

        System.out.println(" ");
        System.out.println(" ");
        int numeroc=0;
        //En este for iniciamos las simulación por día.
        for (int i=0; i<dias;i++){
            System.out.println("Dia número "+(i+1));
            if (movimiento && i!=0){
                
                //En las siguientes lineas sacamos al ciudadano revelado de la carcel
                for (int x=0; x < matriz.length; x++) {
                    for (int y=0; y < matriz[x].length; y++) {
                      if (matriz[x][y]==null && carcel.size()!=0){
                          Persona per= carcel.get(0);
                          per.setPosicionX(x);
                          per.setPosicionY(y);
                          matriz[x][y]=per;
                          carcel.remove(0);
                      }
                    }
                }
                
                //Con el sigguiente for hacemos que por cada iteracion de días los policias encarcelen solo a 1 
                //siudadano revelado
                for (int x=0; x < matriz.length; x++) {
                    for (int y=0; y < matriz[x].length; y++) {
                      if (matriz[x][y] instanceof Policia){
                          Persona pol=matriz[x][y];
                          Policia poli= (Policia)pol;
                          poli.encarcelar();
                      } else if (matriz[x][y] instanceof Ciudadano){
                          Persona per= matriz[x][y];
                          Ciudadano ciu= (Ciudadano)per;
                          ciu.setRevelarse();
                      }
                    }
                }
                
                
                movimiento();//hacemos que los agentes dentro del universo se muevan
                
                
                
                numeroc=carcel.size();
                
            }
            
            //En este for Imprimimos la matriz y los cambios que ha sufrido
            for (int x=0; x < matriz.length; x++) {
                    for (int y=0; y < matriz[x].length; y++) {
                      System.out.print (matriz[x][y]);
                      if (y!=matriz[x].length-1) System.out.print("\t");
                      if (y==matriz[x].length-1)System.out.println("\n");
                    }
            }
            //Imprimimos los ciudadanos encarcelados
            System.out.println("Encarcelados:");
                for (Persona per: carcel){
                    System.out.println(per);
                }
        
            System.out.println("-----------------------------------------------");
            setDiaActual(i);
            es.append(i+1);
            es.append(',');
            es.append(numeroCiudadanoNR());
            es.append(',');
            es.append(numeroCiudadanoR());
            es.append(',');
            es.append(numeroc);
            es.append('\n');    
            System.out.println("----------------------------------------------------------------------------------");
        }
        ak.write(es.toString());
        ak.close();
        System.out.println("Archivo de simulación generado");
        
    }
   
    
    public void generadorPolicia(ArrayList<Persona> policias, ArrayList<Posicion> posiciones, int n){
        boolean respuesta=true;
            while(respuesta){
                Random r1= new Random();
                int x=r1.nextInt((n));
                Random r2=new Random();
                int y=r2.nextInt((n));
                Posicion pos= new Posicion(x,y);
                ArrayList numero= new ArrayList<>();
                for (Posicion posi: posiciones){
                    if (posi.getX()==x && posi.getY()==y){
                        numero.add(1);
                    }        
                }
                if (numero.size()==0){
                    posiciones.add(pos);
                    policias.add(new Policia(x,y));
                    respuesta=false;
                    break;
                }
        }
    
    
    }
    
    public void generadorCiudadano(ArrayList<Persona> ciudadanos, ArrayList<Posicion> posiciones, int n){
        boolean respuesta=true;
            while(respuesta){
                Random r1= new Random();
                int x=r1.nextInt((n));
                Random r2=new Random();
                int y=r2.nextInt((n));
                Posicion pos= new Posicion(x,y);
                ArrayList numero= new ArrayList<>();
                for (Posicion posi:posiciones){
                    if(posi.getX()==x && posi.getY()==y){
                        numero.add(1);
                    }
                }
                if (numero.size()==0){
                    posiciones.add(pos);
                    ciudadanos.add(new Ciudadano(x,y));
                    respuesta=false;
                    break;
                }
            }
    
    
    }
    
    public static void movimiento(){
        ArrayList<Posicion> posicionesNull= new ArrayList<>();
        ArrayList<Persona> amover= new ArrayList<>();
        for (int x=0; x < Universo.matriz.length; x++) {
          for (int y=0; y < Universo.matriz[x].length; y++) {
            if(Universo.matriz[x][y] == null){
                Posicion pos=new Posicion(x,y);
                posicionesNull.add(pos);
            }
          }
        }
        int movimientos=posicionesNull.size();
        for (int i=0;i<movimientos;i++){
            Random r1= new Random();
            Random r2= new Random();
            int x= r1.nextInt(getDimension()-1);
            int y= r2.nextInt(getDimension()-1);
            if (matriz[x][y] instanceof Persona){
                Posicion pos=posicionesNull.get(i);
                int fila=pos.getX();
                int colum=pos.getY();
                matriz[fila][colum]=matriz[x][y];
                matriz[x][y]=null;
            }  
        }
            
        
    }
    

    
    
    
}